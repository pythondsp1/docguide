# digcom.pyx

import numpy as np 
from scipy.special import erfc


cdef class TheoryBER:
    """ This class implements the analytical-BER results of various
        digital communication systems
    """
    cdef: 
        double[:] SNRdB
        int snrRange

    def __init__(self, double[:] SNRdB):
        self.SNRdB = SNRdB
        self.snrRange = len(self.SNRdB)

    cpdef double[:] bpskBER(self):
        """ Theoretical BPSK BER
            
            **Example**: Plot theoretical BER

            .. code-block:: python
                :linenos:
                :emphasize-lines: 7-10
                
                import numpy as np
                from digcom import TheoryBER

                # SNR in dB : -5 to 5
                SNRdB = np.array(np.arange(-5, 5, 1),float)

                # create object of class TheoryBER
                thBER = TheoryBER(SNRdB)
                # invoke bpskBER method
                theoryBER  = thBER.bpskBER()

                for i in range(len(SNRdB)):
                    print("%.2f" % theoryBER[i], end = ", ")
                # output: 0.21, 0.19, 0.16, 0.13, 0.10, 0.08, 0.06, 0.04, 0.02, 0.01,
        """
        cdef:
            int i
            double[:] SNR = np.zeros(self.snrRange, float)
            double[:] theoryBER = np.zeros(self.snrRange,float)

        for i in range(self.snrRange):
            SNR[i] = 10**(self.SNRdB[i]/10) # SNR_db to SNR conversion

        for i in range(self.snrRange):
            theoryBER[i] = 0.5*erfc(SNR[i]**0.5)

        return theoryBER


cdef class SignalGenerator:
    """ This class generates symbols for different systems,
        e.g.  BPSK and QPSK etc.
    """
    cdef: 
        int bitLength
        double[:] b

    def __init__(self, int bitLength):
        self.bitLength = bitLength
        self.b = np.random.uniform(-1, 1, bitLength)

    cpdef int[:] bpskSignal(self):
        """ BPSK signal generator
        
            **Example**: code to generate BPSK signals

            .. code-block:: python
                :linenos:
                :emphasize-lines: 6-9 

                from digcom import SignalGenerator

                # generate 10 bits
                total_bits  = 10

                # create object of class SignalGenerator
                sGen = SignalGenerator(total_bits)
                # invoke bpskSignal method
                signal  = sGen.bpskSignal()

                for i in range(total_bits):
                    print(signal[i], end = " ") 
                # output: -1 -1 1 -1 -1 1 1 -1 -1 1
        """
        cdef: 
            int i
            int[:] signal = np.zeros(self.bitLength, int)

        for i in range(self.bitLength):
            if self.b[i] < 0:
                signal[i]=-1
            else:
                signal[i]=1
        return signal