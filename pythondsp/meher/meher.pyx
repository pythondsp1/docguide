# digcom.pyx

import numpy as np 
from scipy.special import erfc


cdef class MeherDecisionSignal:
    """ Make decision (or detected signal) based on received signal
    """
    
    cdef:
        double[:] rxSignal
        int[:] decisionSignal
        int bitLength

    def __init__(self, double[:] rxSignal):
        self.rxSignal = rxSignal
        self.bitLength = len(self.rxSignal) 
        self.decisionSignal = np.zeros(self.bitLength, int)

    cpdef int[:] bpskDecision(self):
        """ Decision for BPSK signal.
            
            if received_signal > 0 then decision = 1, otherwise -1

            **Example:** Make decision based on received signal 
            
            .. code-block:: python
                :linenos:
                :emphasize-lines: 10-13

                import numpy as np
                import digcom as dc

                num_bits = 5

                rxSignal = np.random.randn(num_bits)
                print(rxSignal)
                # [-0.16363478  0.74166761  0.46336903  0.77690376 -1.80753004]

                # create object of class DecisionSignal
                ds = dc.DecisionSignal(rxSignal)
                # invoke bpskDecision method 
                decisionSignal = ds.bpskDecision()

                for i in range(num_bits):
                    print(decisionSignal[i], end = " ")
                # -1 1 1 1 -1
        """
        cdef:
            int i

        for i in range(self.bitLength):
            if self.rxSignal[i] < 0:
                self.decisionSignal[i]=-1
            else:
                self.decisionSignal[i]=1
        return self.decisionSignal


cdef class MeherErrorCalcualtion:
    """ Error calculation based on decision and original signal"""
    cdef:
        int bitLength, error
        int[:] originalSignal, decisionSignal, errorMatrix

    def __init__(self, int[:] originalSignal, int[:] decisionSignal):
        self.originalSignal = originalSignal
        self.decisionSignal = decisionSignal
        self.bitLength = len(self.originalSignal)
        self.errorMatrix = np.zeros(self.bitLength, int)
        self.error = 0

    cpdef int bpskError(self):
        """ Calculate error for BPSK signal
        
            **Example:** calculate error 
            
            .. code-block:: python
                :linenos:
                :emphasize-lines: 7-10 

                import numpy as np
                import digcom as dc

                o_sig = np.array([1, 1, 1, -1, 1, 1])
                d_sig = np.array([-1, -1, 1, 1, 1, 1])
                
                # create object of class ErrorCalcualtion
                e = dc.ErrorCalcualtion(o_sig, d_sig)
                # invoke bpskError method
                error = e.bpskError()

                print("error: ", error) # error:  3
        """
        cdef: 
            int i

        for i in range(self.bitLength):
            self.errorMatrix[i] =  abs((self.decisionSignal[i] - self.originalSignal[i])/2)
        
        #add all 1's to find total error
        self.error =  np.sum(self.errorMatrix)   
        return self.error