digcom.SignalGenerator
======================

.. currentmodule:: digcom

.. autoclass:: SignalGenerator

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~SignalGenerator.bpskSignal
   
   

   
   
   